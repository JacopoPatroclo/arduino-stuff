var sensor = require("node-dht-sensor");
 
sensor.read(11, 21, function(err, temperature, humidity) {
  if (!err) {
    console.log(`temp: ${temperature}, humidity: ${humidity}%`);
  }
});
