const express = require('express')
const state_manager = require('./src/state_manager')
const { join } = require('path')
const temp_sensor = require('./src/gestore_sensore_temp')
const pers_sensor = require('./src/gestore_sensore_pers')

// Creiamo l'istanza del web server
const server = express()

// Rendiamo "pubblica" la directory "public"
// Il web server invia quando viene richiesto ogni file contenuto in public
server.use(express.static(join(__dirname, 'public')))

// Se la richiesta e' "/api/state" rispondi con lo stato dell'applicatione
server.get('/api/state', (request, response) => {
    // Estrae lo stato dallo state manager
    const state = state_manager.getStato()
    // Risponde serializzando lo stato come json
    response.json(state)
})

// Avvio il web server sulla porta 3000
server.listen(3000, () => {
    // A server avviato, inizio a prendere dati dal sensore di temperatura
    temp_sensor.start()
    pers_sensor.start()
    console.log('Server started')
})
