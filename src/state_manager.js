// Gestore dello stato applicativo

// Costante che rappresenta il massimo numero di persone ammesse
const MAX_PEOPLE = 50

// Oggetto che descrive lo stato applicativo
const stato = {
    numero_persone: 0,
    porte_bloccate: false,
    temperature: 0,
    umidita: 0
}

// Callback di notifica quando le porte vengono bloccate o sbloccate
let porteBloccate;

module.exports = {

    // Funzione che ritorna un clone dello stato applicativo
    getStato() {
        return { ...stato }
    },

    // Funzione che registra il callback per il cambio di stato delle porte
    onPorteBloccate(fn) {
        porteBloccate = fn
    },

    // Funzione che rappresenta l'ingresso di una persona
    enterPerson() {
        console.log('Entra una persona')

        // Incremento del numero di persone
        stato.numero_persone++;

        // Verifico che il conteggio delle persone sia maggiore del massimo consentito
        if (stato.numero_persone >= MAX_PEOPLE) {
            
            // Se si, blocco le porte
            stato.porte_bloccate = true;

            // Se il callback blocco porte e' impostato
            if (porteBloccate) {

                // notifico il nuovo stato delle porte
                porteBloccate(stato.porte_bloccate)
            }
        }
    },

     // Funzione che rappresenta l'uscita di una persona
    leavePerson() {
        console.log('Esce una persona')

        // Decremento del numero di persone
        stato.numero_persone--;

        // Verifico che il conteggio delle persone sia minore del massimo consentito
        if (stato.numero_persone < MAX_PEOPLE) {

            // Se si, sblocco le porte
            stato.porte_bloccate = false

            // Se il callback blocco porte e' impostato
            if (porteBloccate) {

                // notifico il nuovo stato delle porte
                porteBloccate(stato.porte_bloccate)
            }
        }
    },

    // Funzione che permette l'aggioramento del valore dell'umidita'
    aggiornaUmidita(val) {
        stato.umidita = val
    },

    // Funzione che permette l'aggioramento del valore di temperatura
    aggiornaTemperatura(val) {
        stato.temperature = val
    }
}