const gpio = require("rpi-gpio");
const stateManager = require("./state_manager");

module.exports = {
  start: () => {

    // Quando uno dei pin cambia valore
    gpio.on("change", function (pin, value) {

      // Se si tratta del pin 32 (sensonre in ingresso)
      if (pin === 32) {

        // Entra una persona
        if (value) {
          stateManager.enterPerson();
        }

      } else {

        // Esce una persona
        if (value) {
          stateManager.leavePerson();
        }
      }
    });

    // Sensore di uscita
    gpio.setup(12, gpio.DIR_IN, gpio.EDGE_BOTH);

    // Sensore di ingresso
    gpio.setup(32, gpio.DIR_IN, gpio.EDGE_BOTH);
  },
};
