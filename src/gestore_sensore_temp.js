const sensor = require("node-dht-sensor");
const stateManager = require('./state_manager')

// Funzione di avvio lettura
function read() {

    // Imposto un timeout ogni sei secondi
    setTimeout(() => {

        // Leggo il valore dal pin 21 aspettandomi un sensore di tipo DHT11
        sensor.read(11, 21, function(err, temperature, humidity) {

            // Se non ci sono errori
            if (!err) {

                // Aggiorno lo stato applicativo con la nuova temperatura
                stateManager.aggiornaTemperatura(temperature)

                // Aggiorno lo stato applicativo con la nuova umidita'
                stateManager.aggiornaUmidita(humidity)
            }
        });

        // Richiamo ricorsivamente la funzione di lettura,
        // in modo da creare un ciclo infinito di timeout -> lettura -> timeout -> ...
        read()
    },
    // 2000ms = 2 Secondi 
    2000)
}

// Esporto il modulo
module.exports = {
    start: () => {
        read()
    }
}